# cs250-lab4 by Race Mahoney and Schuyler Vetrys

# Running Program
To run the program, run these commands:
'cd src'
'javac -d ../classes SongSort.java'
'cd ../classes'
'java SongSort'

# What is the program
Once the program has begun, you will be prompted to chose what key you would
like sort by, name, artist, genre, year or rank. You can also type 'help' to see
the list of genres and their ranking value. Once the key has been chosen, you will
be prompted to either view the current unsorted array or to begin sorting. If you
choose to sort, you then finally be asked how you would be to sort, either by
Quick Sort or Merge Sort. The resulting sorted array will be then written to the
text file with the name of the type of sort.

# Time Complexities
## Quick Sort
    Best Case - O(n log n)
    Average Case - O(n log n)
    Worst Case - O(n^2)

## Merge sort
    Best Case - O(n log n)
    Average Case - O(n log n)
    Worst Case - O(n log n)
