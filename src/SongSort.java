import java.util.*;
import java.io.*;

public class SongSort{

public static void main(String args[]){
    //create array based on number of days
    Scanner scan  = new Scanner(System.in);
    String[]songInfo = new String[30];
    //store values in an ArrayList
    String[]arr = new String[30];
    ArrayList<String> genres = new ArrayList<String>();
    boolean sortbyGenre = false;
    String genreChoice = "";

    //create strings to hold indivual attributes
    String songName = "";
    String songArtist = "";
    String songGenre = "";
    String songYear = "";
    String songRank = "";

    BufferedReader rd = null;
        try {
            rd = new BufferedReader(new FileReader(new File("songPlaylist.txt")));
          // Read all contents of the file.
            String inputLine = null;

            //add correct number of prices to prices array
            for(int i = 0; i < songInfo.length; i++){
              inputLine = rd.readLine();
              if(inputLine != null){
                songInfo[i] = inputLine;

              }
            }
        }
        catch(IOException ex) {
            System.err.println("An IOException was caught!");
            ex.printStackTrace();
        }
        finally {
            // Close the file.
            try {
                rd.close();
            }
            catch (IOException ex) {
                System.err.println("An IOException was caught!");
                ex.printStackTrace();
            }
        }


        //get user input
        //keep asking until
        String sortID = " ";
        do{
          System.out.println("How would you like to sort yo tunes?");
          System.out.println("(name,artist,genre,year,rank)           [For list of genres type 'help']");
          System.out.print(">");
          sortID = scan.nextLine();

          //dislpay the list of genres if the user needs help
          if(sortID.equals("help")){
            System.out.println("\nGENRES\n---------------------\nAlternative Rock [1]\nDance [2]\nPop [3]\nR&B [4]\nRap [5]\nRock[6]\n---------------------\n");
            sortID = " ";
          }
        } while(sortID.equals(" "));

        if(sortID.equals("genre")){
          System.out.println("What genre do you want to sort by?");
          genreChoice = scan.nextLine();
        }


        int songCount = 0;
        //split up the info from the text file into multiple strings
        for(int i = 0; i < songInfo.length; i++){
          String str = songInfo[i];
          String [] arrStr = str.split(",");
          songName = arrStr[0];
          songArtist = arrStr[1];
          songGenre = arrStr[2];
          songYear = arrStr[3];
          songRank = arrStr[4];

          //put the attrbutes from the txt file into a map with the specificed key
          if(sortID.equals("name")){
            arr[i] = (songName + "," + songArtist + ", " + songGenre + ", " + songYear + ", " + songRank);
          }

          if(sortID.equals("artist")){
            arr[i] = (songArtist + "," + songName + ", " + songGenre + ", " + songYear + ", " + songRank);
          }

          if(sortID.equals("genre")){
            sortbyGenre = true;
            arr[i] = (songGenre + "," + songName + ", " + songArtist + ", " + songYear + ", " + songRank);
          }

          if(sortID.equals("year")){
            arr[i] = (songYear + "," + songName + ", " + songArtist + ", " + songGenre + ", " + songRank);
          }

          if(sortID.equals("rank")){
            arr[i] = (songRank + "," + songName + ", " + songArtist + "," + songGenre + "," + songYear);
          }

          //add genres to ArrayList
          if(!genres.contains(songGenre)){
            genres.add(songGenre);
          }

          if(songGenre.equals(genreChoice)){
            songCount++;
          }
        }


        //make the array only contain songs of that genre
        String[] arrG = new String[songCount];
        int index = 0;
        String tempGenre;
        //only if user has specificed to sort by genre
        if(sortbyGenre){

          for(int i = 0; i < arr.length; i++){
            String str = songInfo[i];
            String [] arrStr = str.split(",");
            songName = arrStr[0];
            songArtist = arrStr[1];
            songGenre = arrStr[2];
            songYear = arrStr[3];
            songRank = arrStr[4];

            if(songGenre.equals(genreChoice)){
                arrG[index] = (songName + "," + songArtist + ", " + songGenre + ", " + songYear + ", " + songRank);
                index++;
            }
          }
        }

        int n = arr.length;
        String choice = " ";
        String sortChoice = " ";
        do{
          System.out.println("\nType 'view' to see the song list before being sorted\nType 'sort' to begin sorting the array");
          System.out.print(">");
          choice = scan.nextLine();

          if(choice.equals("view")){
            //display
              System.out.println("\nBEFORE\n");
                if(!sortbyGenre){
                  for(int i = 0; i < arr.length; i++){
                    System.out.println(arr[i]);
                  }
                } else {
                    for(int i = 0; i < arrG.length; i++){
                      System.out.println(arrG[i]);
                    }
                }
                choice = " ";
            }

          if(choice.equals("sort")){
            System.out.println("Which sorting method would you like to use? 'Quick Sort' or 'Merge Sort'?");
            System.out.print(">");
            sortChoice = scan.nextLine();

              if(sortChoice.equals("Quick Sort")){
                if(!sortbyGenre){
                  quickSort(arr,0,n-1);
                } else {
                  quickSort(arrG,0,songCount-1);
                }

              }
              if(sortChoice.equals("Merge Sort")){
                if(!sortbyGenre){
                  mergeSort(arr,0, n);
                } else {
                  mergeSort(arrG,0,songCount);
                }

              }

        }

        } while(choice.equals(" "));

        WriteData(arr, sortChoice, sortID);
        System.out.println("The data has been sorted by " + sortID + " and is saved in a text file named " + sortChoice + ".txt");

  }//main

  private static void quickSort(String[] arr, int low, int high){
    if(arr.length == 0){
      return;
    }

    int i = low;
    int j = high;

    if(j - i >= 1)
    {
      //set pivot point at the highest index
      String pivot = arr[i];

      while(j > i)
      {
        //staring from the left, if the current element is less than the piviot
        //then keep moving and stop when we reach an element greater than the piviot
        while(arr[i].compareTo(pivot) <= 0 && i < high && j > i) {
          i++;
        }

        //staring from the right, if the current element is greater than the piviot
        //then keep moving and stop when we reach an element less than the piviot
        while(arr[j].compareTo(pivot) >= 0 && j > low && j >= i) {
          j--;
        }

        if(j > i){
          //swap the elements if j is larger than i
          String temp = arr[i];
          arr[i] = arr[j];
          arr[j] = temp;
        }

      }
      //swap the piviot and the highest element
      String temp = arr[low];
      arr[low] = arr[j];
      arr[j] = temp;

      //with new high and low points
      //recursively call quickSort until
      //the array is sorted completely
      quickSort(arr, low, j-1);
      quickSort(arr, j+1, high);
    }
    }

  private static void merge(String[] arr, int start, int n1, int n2){
   String [] temp = new String [n1+n2];
    int node = 0;
    int node1 = 0;
    int node2 = 0;

    while((node1 < n1) && (node2 < n2)){
      if(arr[start + node1].compareTo(arr[start + n1 + node2]) <0)
        temp[node++] = arr[start + (node1++)];
        else
          temp[node++] =  arr[start + n1 +(node2++)];
        }
        while(node1 <n1)
          temp[node++] = arr[start + (node1++)];
        while(node2 <n2)
          temp[node++] = arr[start + n1 + (node2++)];

          for(int i =0; i< node; i++)
          arr[start +i] = temp[i];

  } //M

  public static void mergeSort(String [] arr, int start, int n){
    int n1 = 0;
    int n2 = 0;

    if(n>1)
    {
      n1 = n/2;
      n2 = n-n1;

      mergeSort(arr, start, n1);
      mergeSort(arr, start + n1, n2);
    }

    merge(arr, start, n1, n2);

    for(String element: arr)
      System.out.print(element + " ");

  } //MS


      private static void WriteData(String arr[], String destination, String sortID){
        //create new File object based on which sort was done
        //If quick Sort was chosen write to that file
        if(destination.equals("Quick Sort")){
          try{
            BufferedWriter bw = new BufferedWriter(new FileWriter("QuickSortResults.txt"));

            bw.write(sortID.toUpperCase() + " SORTED BY THE POWER OF " + destination.toUpperCase() + "!!!\n\n");
            for(int i = 0; i < arr.length; i++){
              bw.write(arr[i]);
              bw.write("\n");
              }
              bw.flush();
              bw.close();

              } catch (IOException e){
                  e.printStackTrace();
              }
        }

        //if merge sort chosen write to that file
        if(destination.equals("Merge Sort")){
          try{
            BufferedWriter bw = new BufferedWriter(new FileWriter("MergeSortResults.txt"));

            bw.write(sortID.toUpperCase() + " SORTED BY THE POWER OF " + destination.toUpperCase() + "!!!\n\n");
            for(int i = 0; i < arr.length; i++){
              bw.write(arr[i]);
              bw.write("\n");
              }
              bw.flush();
              bw.close();

              } catch (IOException e){
                  e.printStackTrace();
              }
        }
      }


}//songsor
